
const express = require('express')
const app = express()
require('./db.js')



app.use(express.json())

process.on('uncaughtException', function (err) {
    console.log('uncaughtException: ', err)
})

// ############ Routers ############
const { getMoviesController, searchMoviesController,
    addMoviesController, updateMoviesController, deleteMovieController
} = require('./controllers/movies.js')
const { isAdmin } = require('./middlewares/isAdmin')

app.get('/', function (req, res) {
    res.send('Health check')
})


app.get('/movies', getMoviesController)

app.get('/search', searchMoviesController)

app.post('/movies', isAdmin, addMoviesController)

app.put('/movies/:id', isAdmin, updateMoviesController)

app.delete('/movies/:id', isAdmin, deleteMovieController)




const PORT = process.env.PORT
app.listen(PORT, () => { console.log('Server stated at port', PORT) })