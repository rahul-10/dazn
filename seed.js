const mongoose = require('mongoose');
const User = require('./models/user');
const { ROLES } = require('./configs/enums')

require('dotenv').config();

const { MONGODB_URI } = process.env;

mongoose.connect(MONGODB_URI).then(() => console.log('Connected!'));


const db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));


db.once('open', async () => {
    // Remove  data
    await db.collections['users'].drop(function (err) {
        console.log('Users collection dropped');
    });
    // Seed data
    const users = [
        { name: 'Rahul', role: ROLES.ADMIN },
        { name: 'Shyam' },
    ];

    try {
        await User.insertMany(users);
        console.log('Data seeded successfully');
    } catch (err) {
        console.error('Error seeding data:', err);
    } finally {
        mongoose.connection.close();
    }
});
