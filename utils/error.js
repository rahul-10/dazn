function CustomError(statusCode, message) {
    const error = new Error(message);

    // Assign the name property
    error.name = 'CustomError';

    // Assign additional properties
    error.statusCode = statusCode || 500;

    // Capture the stack trace, excluding the constructor call from the stack
    Error.captureStackTrace(error, CustomError);

    return error;
}

module.exports = CustomError;