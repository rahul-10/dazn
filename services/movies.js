const Movie = require('../models/movie')
const CustomError = require('../utils/error')


exports.getMovies = ({ title, genre }) => {
    const condition = {}
    if (title) {
        condition.title = title
    }
    if (genre) {
        condition.genre = genre
    }

    return Movie.find(condition)
}

exports.addMovie = async (data) => {
    const { title, genre, rating, streaming_link } = data
    try {
        if (!title || !genre) {
            throw new CustomError(400, 'title and genre are mandatory');
        }
        const result = await Movie.create(data)
        return result;
    } catch (err) {
        throw err
    }
}

exports.updateMovie = async (id, data) => {
    try {
        // remaining - title & genre should be null
        const result = await Movie.updateOne(
            { _id: id },
            { $set: data }
        );
        return 'Updated';
    } catch (err) {
        throw err
    }
}

exports.deleteMovie = async (id) => {
    try {
        await Movie.deleteOne({ _id: id });
        return 'Deleted';
    } catch (err) {
        throw err
    }
}