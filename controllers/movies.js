const { getMovies, addMovie, updateMovie, deleteMovie } = require('../services/movies')

exports.getMoviesController = async (req, res) => {
    try {
        const result = await getMovies()
        res.status(200).json({ status: 200, data: result })
    } catch (err) {
        const statusCode = err.statusCode || 500
        res.status(statusCode).json({
            status: statusCode,
            data: err.message || 'Something went wrong'
        })
    }
}

exports.searchMoviesController = async (req, res) => {
    try {

        const { title, genre } = req.query
        const result = await getMovies({ title, genre })
        res.status(200).json({ status: 200, data: result })
    } catch (err) {
        const statusCode = err.statusCode || 500
        res.status(statusCode).json({
            status: statusCode,
            data: err.message || 'Something went wrong'
        })
    }
}

exports.addMoviesController = async (req, res) => {
    try {
        const data = req.body
        const result = await addMovie(data)
        res.status(201).json({ status: 201, data: result })
    } catch (err) {
        const statusCode = err.statusCode || 500
        res.status(statusCode).json({
            status: statusCode,
            data: err.message || 'Something went wrong'
        })
    }
}


exports.updateMoviesController = async (req, res) => {
    try {
        const id = req.params.id
        const data = req.body
        const result = await updateMovie(id, data)
        res.status(200).json({ status: 200, data: result })
    } catch (err) {
        const statusCode = err.statusCode || 500
        res.status(statusCode).json({
            status: statusCode,
            data: err.message || 'Something went wrong'
        })
    }
}



exports.deleteMovieController = async (req, res) => {
    try {
        const id = req.params.id
        const result = await deleteMovie(id)
        res.status(204).json()
    } catch (err) {
        const statusCode = err.statusCode || 500
        res.status(statusCode).json({
            status: statusCode,
            data: err.message || 'Something went wrong'
        })
    }
}