const mongoose = require('mongoose');

const { addMovie } = require('../services/movies');
const Movie = require('../models/movie')

jest.mock('../models/movie');

describe('Add Movie', () => {
    afterAll(() => {
        mongoose.connection.close();
    });

    test('Should throw 400 error when title is missing', async () => {
        const data = {
            genre: 'Action',
            rating: 4,
            streaming_link: 'http://test.dummy.com'
        }
        await expect(addMovie(data))
            .rejects
            .toThrow('title and genre are mandatory');
    });

    test('Should add movie when every field is present', async () => {
        const data = {
            title: 'test movie',
            genre: 'Action',
            rating: 4,
            streaming_link: 'http://test.dummy.com'
        }
        Movie.create.mockResolvedValue(data);

        const result = await addMovie(data)

        expect(Movie.create).toHaveBeenCalledWith(data);
        expect(result).toEqual(data);

    });

});
