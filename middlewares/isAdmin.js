const User = require('../models/user')
const CustomError = require('../utils/error')
const { ROLES } = require('../configs/enums')

exports.isAdmin = async (req, res, next) => {
  const name = req.headers.user;
  if (!name) {
    res.status(400).json({
      status: 400,
      data: 'user in headers is mandatory'
    })
  }
  try {
    const result = await User.findOne({ name })
    if (result?.role !== ROLES.ADMIN) {
      throw new CustomError(403, 'You are not allowed to perform this action')
    }
  } catch (err) {
    res.status(err.statusCode || 500).json({
      status: err.statusCode || 500,
      data: err.message || 'something went wrong'
    })
  }

  next();
};
