# dazn

## Clone this project
```
1. git clone git@gitlab.com:rahul-10/dazn.git
2. Update .env file as per your config
3. npm install
```

## Run seed file

Run seed file to onboard two hard coded user Rahul * Shyam. Rahul has admin access. To add, update & delete movie use header --> user: Rahul 
Command to run seed file: npm run seed


## Run Project
Command: npm start

## Test
Added 2 tests only because of time crunch.
Command: npm test

