const mongoose = require('mongoose');
const { ROLES } = require('../configs/enums')

const userSchema = new mongoose.Schema({
    _id: { type: String },
    name: { type: String, required: true, index: true },
    role: { type: String, enum: [ROLES.USER, ROLES.ADMIN], default: ROLES.USER },
});

const User = mongoose.model('User', userSchema);

module.exports = User;
