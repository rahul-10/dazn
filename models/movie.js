const mongoose = require('mongoose');

const movieSchema = new mongoose.Schema({
    title: { type: String, required: true, index: true },
    genre: { type: String, required: true, index: true },
    rating: { type: String },
    streaming_link: { type: String },
});

const Movie = mongoose.model('Movie', movieSchema);

module.exports = Movie;
